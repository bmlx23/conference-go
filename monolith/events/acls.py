# This file contains code that will be used to make HTTP requests to Pexels and OpenWeatherMap
# Any code here should return Python dictionaries to be used by our view code.
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

def get_picture_url(city, state):

    url = f'https://api.pexels.com/v1/search?query={city} {state}'
    pexel_headers = {"Authorization":PEXELS_API_KEY}
    image_response = requests.get(url, headers=pexel_headers)
    image_url_dict = json.loads(image_response.content)
    # print(type(image_response.json()))
    image_url = image_url_dict["photos"][0]["src"]["original"]
    return image_url

def get_weather_data(city, state):
    # First get Geo Codes of Latitude and Longitude
    geo_url = f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},ISO3166&limit=1&appid={OPEN_WEATHER_API_KEY}'
    geo_response = requests.get(geo_url)
    geo_url_dict = geo_response.json()
    print(geo_url_dict)
    geo_lat = geo_url_dict[0]["lat"]
    geo_lon = geo_url_dict[0]["lon"]
    # print(geo_lat)
    # Now use geo_lat and geo_lon to get weather data
    weather_url = f'https://api.openweathermap.org/data/2.5/weather?lat={geo_lat}&lon={geo_lon}&appid={OPEN_WEATHER_API_KEY}'
    weather_response = requests.get(weather_url)
    weather_dict = weather_response.json()
    weather_data = {
        "temperature": weather_dict["main"]["temp"],
        "description": weather_dict["weather"][0]["description"]
        }
    return weather_data
