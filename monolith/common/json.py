from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet

class DateEncoder(JSONEncoder):
    def default(self, o):
        # if o is an instance of datetime
        #    return o.isoformat()
        # otherwise
        #    return super().default(o)
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)

class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)

class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {} # add this when adding encoders to properties
    def default(self, o):
        #   if the object to decode is the same class as what's in the
        #   model property, then
        if isinstance(o, self.model):
            dictionary = {}
        #     * create an empty dictionary that will hold the property names
        #       as keys and the property values as values
        #     * for each name in the properties list
            # if o has the attribute get_api_url
            #    then add its return value to the dictionary
            #    with the key "href"
    # if getattr(o, "get_api_url") != None: another way to check if o has get_api_url attribute
            if hasattr(o, "get_api_url"):
                 dictionary['href'] = o.get_api_url()
            for prop in self.properties:
        #         * get the value of that property from the model instance
        #           given just the property name
                value = getattr(o, prop)
                if prop in self.encoders:
                    encoder = self.encoders[prop] #don't forget to add encoders = {} above def
                    value = encoder.default(value)
        #         * put it into the dictionary with that property name as
        #           the key
                dictionary[prop] = value
            dictionary.update(self.get_extra_data(o))
            return dictionary
        #   otherwise,
        else:
            return super().default(o)
        #       return super().default(o)  # From the documentation

    def get_extra_data(self, o):  # this is for edge case data
        return {}
